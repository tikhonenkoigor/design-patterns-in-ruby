# Design patterns in ruby

* [Template method](#template-method)
* [Strategy](#strategy)
* [Observer](#observer)
* [Composite](#composite)
* [Iterator](#iterator)
* [Commands](#commands)
* [Adapter](#fdapter)
* [Proxy](#proxy)
* [Decorator](#decorator)
* [Singleton](#singleton)
* [Factory](#factory)
* [Builder](#builder)
* [Interceptor](#interceptor)

## Template method

The general idea of the Template Method pattern is to build an abstract base class (`Report`) with a skeletal method.

In the Template Method pattern, the abstract base class (`Report`) controls the higher-level processing through the template method (`output_report`). The sub- classes simply fill in the details.

Non-abstract methods that can be overridden in the concrete classes of the Template Method pattern are called `hook methods`. Hook methods permit the concrete classes to choose (1) to override the base implementation and do something different or (2) to simply accept the default implementation.

Hook methods: `[output_start output_body_start output_body_end output_end]`

```ruby
class Report
  def initialize
    @title = 'Monthly Report'
    @text =  ['Things are going', 'really, really well.']
  end

  def output_report
    output_start
    output_head
    output_body_start
    output_body
    output_body_end
    output_end
  end

  def output_body
    @text.each do |line|
      output_line(line)
    end
  end

  def output_start; end

  def output_head
    raise 'Called abstract method: output_head'
  end

  def output_body_start; end

  def output_line(line)
    raise 'Called abstract method: output_line'
  end

  def output_body_end; end

  def output_end; end
end

class HTMLReport < Report
  def output_start
    puts('<html>')
  end

  def output_head
    puts('<head>')
    puts("<title>#{@title}</title>")
    puts('</head>')
  end

  def output_body_start
    puts('<body>')
  end

  def output_line(line)
    puts("<p>#{line}</p>")
  end

  def output_body_end
    puts('</body>')
  end

  def output_end
    puts('</html>')
  end
end

class PlainTextReport < Report
  def output_start; end
  def output_head
    puts("**** #{@title} ****")
    puts
  end

  def output_body_start; end

  def output_line(line)
    puts(line)
  end

  def output_body_end; end

  def output_end; end
end
```

The Template Method pattern is simply a fancy way of saying that if you want to vary an algorithm, one way to do so is to code the invariant part in a base class and to encapsulate the variable parts in methods that are defined by a number of subclasses.

The base class can simply leave the methods completely undefined, in that case, the subclasses must supply the methods. Alternatively, the base class can provide a default implementation for the methods that the subclasses can override if they want.

## Strategy

The key idea underlying the Strategy pattern is to define a family of objects, the strategies, which all do the same thing.

```ruby
class HTMLFormatter
  def output_report( context )
    puts('<html>')
    puts('  <head>')
    puts("    <title>#{context.title}</title>")
    puts('  </head>')
    puts('  <body>')
    context.text.each do |line|
      puts("    <p>#{line}</p>" )
    end
    puts('  </body>')
    puts('</html>')
  end
end

class PlainTextFormatter
  def output_report(context)
    puts("***** #{context.title} *****")
    context.text.each do |line|
      puts(line)
    end
  end
end

class Report
  attr_reader :title, :text
  attr_accessor :formatter

  def initialize(formatter)
    @title = 'Monthly Report'
    @text =  [ 'Things are going', 'really, really well.' ]
    @formatter = formatter
  end

  def output_report
    @formatter.output_report( self )
  end
end
```

Using the new Report class

```ruby
report = Report.new(HTMLFormatter.new)
report.output_report
```

Each strategy object perform the same job, and all of the objects support exactly the same interface.

We relieve the Report class of any responsibility for or knowledge of the report file format.

With the Template Method pattern, we make our decision when we pick our concrete subclass. In the Strategy pattern, we make our decision by selecting a strategy class at runtime.

## Observer

The Observer pattern allows you to build components that know about the activities of other components without having to tightly couple everything together in an unmanageable mess of code-flavored spaghetti.

By creating a clean interface between the source of the news (the observable object) and the consumer of that news (the observers), the Observer pattern moves the news without tangling things up.

Most of the work in implementing the Observer pattern occurs in the subject or observable class.

```ruby
# Subject
module Subject
  def initialize
    @observers=[]
  end

  def add_observer(observer)
    @observers << observer
  end

  def delete_observer(observer)
    @observers.delete(observer)
  end

  def notify_observers
    @observers.each do |observer|
      observer.update(self)
    end
  end
end

# Wants to be a Subject
class Employee
  include Subject

  attr_reader :name, :title
  attr_reader :salary

  def initialize(name, title, salary)
    super()
    @name = name
    @title = title
    @salary = salary
  end

  def salary=(new_salary)
    @salary = new_salary
    notify_observers
  end
end

# Observer
class Payroll
  def update( changed_employee )
    puts("Cut a new check for #{changed_employee.name}!")
    puts("His salary is now #{changed_employee.salary}!")
  end
end

# Observer
class TaxMan
  def update( changed_employee )
    puts("Send #{changed_employee.name} a new tax bill!")
  end
end
```

The subject is a class with the news. In our example, the `subject` is the `Employee` class. The observers are the objects that are interested in getting the news.

In our employee example, we have two observers: `Payroll` and `TaxMan`. When an object is interested in being informed of the state of the subject, it registers as an observer on that subject.

## Composite

To build the Composite pattern, you need three moving parts.

1. First, you need a common interface or base class for all of your objects. The GoF call this base
class or interface the `component`.

2. Second, you need one or more `leaf` classes that is, the simple, indivisible building blocks of the process.

3. Third, we need at least one higher-level class, which the GoF call the `composite`
class

The `composite` is a component, but it is also a higher-level object that is built
from subcomponents. In the baking example, the composites are the complex tasks
such as making the batter or manufacturing the whole cake — that is, the tasks that are made up of subtasks.

```ruby
# component base class
class Task
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_time_required
    0.0
  end
end
```
Task is an abstract base class in the sense that it is not really complete: It just keeps
track of the name of the task and has a do-nothing `get_time_required` method.
```ruby
# leaf classe
class AddDryIngredientsTask < Task
  def initialize
    super('Add dry ingredients')
  end

  def get_time_required
    1.0 # 1 minute to add flour and sugar
  end
end

# leaf classe
class MixTask < Task
  def initialize
    super('Mix that batter up!')
  end

  def get_time_required
    3.0 # Mix for 3 minutes
  end
end
```

```ruby
# composite task
class CompositeTask < Task
  def initialize(name)
    super(name)
    @sub_tasks = []
  end

  def add_sub_task(task)
    @sub_tasks << task
  end

  def remove_sub_task(task)
    @sub_tasks.delete(task)
  end

  def get_time_required
    time=0.0
    @sub_tasks.each {|task| time += task.get_time_required}
    time
  end
end

class MakeBatterTask < CompositeTask
  def initialize
    super('Make batter')
    @sub_tasks = []
    add_sub_task( AddDryIngredientsTask.new )
    add_sub_task( AddLiquidsTask.new )
    add_sub_task( MixTask.new )
  end
end
```


## Iterator

The purpose of an iterator is to introduce your code to each sub-object of an aggregate object.

- **External**
```ruby
class ArrayIterator
  def initialize(array)
    @array = array
    @index = 0
  end

  def has_next?
    @index < @array.length
  end

  def item
    @array
  end

  def next_item
    value = @array[@index]
    @index += 1
    value
  end
end
```

And now we can use

```ruby
array = ['red', 'green', 'blue']
i = ArrayIterator.new(array)

while i.has_next?
  puts("item: #{i.next_item}")
end
-------------------------------
i = ArrayIterator.new('abc')

while i.has_next?
  puts("item: #{i.next_item.chr}")
end
```
- **Internal**

When all of the iterating action occurs inside the aggregate object, the code block-based iterators are called `internal iterators`.

```ruby
def for_each_element(array)
  i = 0
  while i < array.length
    yield(array[i])
    i += 1
  end
end
-----------------------------
a = [10, 20, 30]
for_each_element(a) {|element| puts("The element is #{element}")}
```

Merge two sorted arrays using `external iterator`
```ruby
def merge(array1, array2)
  merged = []

  iterator1 = ArrayIterator.new(array1)
  iterator2 = ArrayIterator.new(array2)

  while( iterator1.has_next? and iterator2.has_next? )
    if iterator1.item < iterator2.item
      merged << iterator1.next_item
    else
      merged << iterator2.next_item
    end
  end

  # Pick up the leftovers from array1
  while( iterator1.has_next?)
    merged << iterator1.next_item
  end

  # Pick up the leftovers from array2
  while( iterator2.has_next?)
    merged << iterator2.next_item
  end

  merged
end
```

- **Adding Enumerable to a class**

You need only make sure that your internal iterator method is named each and that the individual elements that you are going to iterate over have a reasonable implementation of the <=> comparison operator.

`Enumerable` will add to your class a whole range of useful methods. Among the handy things you get from `Enumerable` are `include?(obj)`, which returns true if the object supplied as a parameter is part of your aggregate object, plus `min` and `max`, which return exactly what you would expect.

```ruby
class Account
  attr_accessor :name, :balance

  def initialize(name, balance)
    @name = name
    @balance = balance
  end

  def <=>(other)
    balance <=> other.balance
  end
end

class Portfolio
  include Enumerable

  def initialize
    @accounts = []
  end

  def each(&block)
    @accounts.each(&block)
  end

  def add_account(account)
    @accounts << account
  end
end
```

## Commands
## Adapter
## Proxy
## Decorator
## Singleton
## Factory
## Builder
## Interceptor